import './App.css';
import { useState } from 'react';
import { TextInput, Button, NumberInput } from '@mantine/core';

function Form({ sendData }) {
  const [itemName, setItemName] = useState('');
  const [itemQuantity, setItemQuantity] = useState(0);

  const onClick = async () => {
    await sendData(itemName, itemQuantity)
  }

  return (
    <div className='form-container'>
      <div className='inputs-container'>
        <TextInput
          placeholder="Banana"
          label="Item name"
          size="xl"
          withAsterisk
          theme={{ colorScheme: 'dark' }}
          value={itemName} onChange={(event) => setItemName(event.currentTarget.value)}
        />
        <NumberInput
          defaultValue={1}
          label="Quantity"
          size="xl"
          withAsterisk
          theme={{ colorScheme: 'dark' }}
          value={itemQuantity} onChange={(val) => setItemQuantity(val)}
          min={0}
          max={100}
        />
      </div>

      <div style={{ width: 300 }}>
        <Button radius="md" size="xl" variant="gradient" gradient={{ from: 'teal', to: 'blue', deg: 60 }} onClick={onClick}>
          Add to list
        </Button>
      </div>
    </div>
  );
}

function App() {
  const [data, setData] = useState([])
  const API_URL = process.env.REACT_APP_API_URL;

  const sendData = async (itemName, itemQuantity) => {
    try {
      // Default options are marked with *
      const response = await fetch(`${API_URL}/item`, {
        method: 'POST',
        // allow cors
        mode: 'cors',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ name: itemName, quantity: itemQuantity })
      });

      await updateState();
    } catch (error) {
      console.log(error);
    }
  }

  const updateState = async () => {
    try {
      let api_call = await fetch(API_URL);
      let api_body_json = await api_call.json()
      setData(api_body_json);
    } catch (error) {
      console.log(error);
    }
  }

  useState(async () => {
    await updateState();
  }, [])


  let rows = data.map((item) => {
    return (
      <div className='table-content' key={item.name}>
        <div>{item.name}</div>
        <div>{item.quantity}</div>
      </div>
    );
  });

  return (
    <div className="App">
      <header className="App-header">
        <Form sendData={sendData} />
        <div className='table-container'>
          <div className='table-header'>
            <div>Name</div>
            <div>Quantity</div>
          </div>
          <div>{rows}</div>
        </div>
      </header>
    </div>
  );
}

export default App;